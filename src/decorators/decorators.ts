export function logClass(target: any) {
    console.log(`Creating instance of class ${target.name}`);

    const originalDestructor = target.prototype.constructor;

    target.prototype.constructor = function (...args: any[]) {
        console.log(`Destroying instance of class ${target.name}`);
        originalDestructor.apply(this, args);
    };

    return target;
}
