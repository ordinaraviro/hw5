import { Professor } from "../classes/teacher";
import { IProfessor } from "../interfaces/interfaces";

export function isProfessor (person: IProfessor | any): person is Professor {
    return person instanceof Professor;
}
