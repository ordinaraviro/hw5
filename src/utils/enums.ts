export enum ItemCategory {
    Students = 'students',
    Teachers = 'teachers',
    Courses = 'courses',
}
