import { Course } from "./classes/course";
import { Student } from "./classes/student";
import { Professor, Teacher } from "./classes/teacher";
import { University } from "./classes/university";
import { ItemCategory } from "./utils/enums";

const university1 = new University('Cambridge');

const student1 = new Student('W. Wallace', 19);
const student2 = new Student('L. da Vinci', 20);
const student3 = new Student('A. Einstein', 18);
const student4 = new Student('M. Curie', 21);
const student5 = new Student('M. Gandhi', 21);

const teacher1 = new Teacher('Dr. Aristotle', 40, 5000);
const teacher2 = new Teacher('Pr. Ivanov', 40, 5000);
const teacher3 = new Teacher('Dr. Sagan', 40, 5000);
const teacher4 = new Professor('Pr. Vernadskiy', 40, 5000);
const teacher5 = new Professor('Dr. Sullivan', 40, 5000);

const course1 = new Course('Math');
const course2 = new Course('Rocket Science');

university1.addItem(student1, ItemCategory.Students);
university1.addItem(student2, ItemCategory.Students);
university1.addItem(student3, ItemCategory.Students);
university1.addItem(student4, ItemCategory.Students);
university1.addItem(student5, ItemCategory.Students);

university1.addItem(teacher1, ItemCategory.Teachers);
university1.addItem(teacher2, ItemCategory.Teachers);
university1.addItem(teacher3, ItemCategory.Teachers);
university1.addItem(teacher4, ItemCategory.Teachers);
university1.addItem(teacher5, ItemCategory.Teachers);

university1.addItem(course1, ItemCategory.Courses);
university1.addItem(course2, ItemCategory.Courses);

console.log(university1);
console.log('-----');
university1.simulateAcademicYear();
console.log('-----');
console.log(university1);
