import { Lecturer } from "../types/types";
import { ItemCategory } from "../utils/enums";

export interface IPerson {
    name: string;
    age: number;
    ability: number;
}

export interface IStudent extends IPerson {
    learn(): void;
    passExams(university: IUniversity): void;
}

export interface IEducator extends Omit<IPerson, "ability"> {
    salary: number;
    strictness: number;
}

export interface IProfessor extends IEducator {
    doResearch(university: IUniversity, assistant: IStudent): void;
}

export interface ICourse {
    name: string;
}

export interface IUniversity {
    name: string;
    students: IStudent[];
    teachers: Lecturer[];
    courses: ICourse[];
    addItem(item: IStudent | IEducator | ICourse, itemType: ItemCategory): void;
    findItem(itemName: string, itemType: ItemCategory): IStudent | IEducator | ICourse | undefined;
    removeItem(item: IStudent | IEducator | ICourse, itemType: ItemCategory): void;
    simulateAcademicYear(): void;
    getItemsByCategory(itemType: ItemCategory): (IStudent | IEducator | ICourse)[];
}
