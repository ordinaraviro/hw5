import { ICourse } from "../interfaces/interfaces";

export class Course implements ICourse {
    name: string;

    constructor(name: string) {
        this.name = name;
    }
}
