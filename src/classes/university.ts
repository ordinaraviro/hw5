import { logClass } from "../decorators/decorators";
import { IEducator, IStudent, ICourse, IUniversity } from "../interfaces/interfaces";
import { Lecturer } from "../types/types";
import { isProfessor } from "../utils";
import { ItemCategory } from "../utils/enums";

@logClass
export class University implements IUniversity {
    name: string;
    students: IStudent[];
    teachers: Lecturer[];
    courses: ICourse[];

    constructor(name: string) {
        this.name = name;
        this.students = [];
        this.teachers = [];
        this.courses = [];
    }

    addItem(item: IStudent | IEducator | ICourse, itemType: ItemCategory): void {
        if (!this.findItem(item.name, itemType)) {
            this[itemType].push(item as any);
            console.log(`${item.name} added to ${this.name} ${itemType}.`);
        } else {
            console.log(`${item.name} already exists in ${this.name} ${itemType}.`);
        }
    }

    findItem(itemName: string, itemType: ItemCategory): IStudent | IEducator | ICourse | undefined {
        return this[itemType].find((item) => item.name === itemName);
    }

    removeItem(item: IStudent | IEducator | ICourse, itemType: ItemCategory): void {
        const index = this[itemType].indexOf(item as any);
        if (index !== -1) {
            this[itemType].splice(index, 1);
            console.log(`${item.name} removed from ${this.name} ${itemType}.`);
        }
    }

    simulateAcademicYear(): void {
        this.teachers.forEach((teacher, index) => {
            const student = this.students[index];
            if (student && isProfessor(teacher)) teacher.doResearch(this, student);
        });

        this.students.forEach((student) => {
            student.passExams(this);
        });
    }

    getItemsByCategory(itemType: ItemCategory): (IStudent | IEducator | ICourse)[] {
        return this[itemType];
    }
}
