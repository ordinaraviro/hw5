import { IUniversity, IStudent } from "../interfaces/interfaces";
import { ItemCategory } from "../utils/enums";

export class Student implements IStudent {
    name: string;
    ability: number;
    age: number

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
        this.ability = Math.ceil(Math.random() * 100);
    }

    learn(): void {
        this.ability += Math.ceil(Math.random() * 100);
    }

    passExams(university: IUniversity): void {
        if (university.teachers.length === 0 || university.courses.length === 0) {
            console.log(`${university.name} doesn't work. Can't pass exams.`);
        } else {
            let allPassedFlag = true;
            university.courses.forEach((course) => {
                const teacher = university.teachers[Math.floor(Math.random() * university.teachers.length)];
                if (this.ability < teacher.strictness) {
                    allPassedFlag = false;
                    console.log(`${this.name} failed the '${course.name}' exam with ${teacher.name}.`);
                }
            });

            if (!allPassedFlag) {
                console.log(`The ${this.name} has been expelled from the ${university.name} university.`);
                university.removeItem(this, ItemCategory.Students);
            } else {
                console.log(`The ${this.name} successfully passed the exams!`);
                university.removeItem(this, ItemCategory.Students);
            }
        }
    }
}
