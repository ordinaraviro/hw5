import { IStudent, IEducator, IUniversity, IProfessor } from "../interfaces/interfaces";
import { ItemCategory } from "../utils/enums";

export abstract class Educator implements IEducator {
    name: string;
    age: number;
    ability: number;
    strictness: number;
    private _salary: number;

    constructor(name: string, age: number, salary: number) {
        this.name = name;
        this.age = age;
        this._salary = salary;
        this.ability = Math.ceil(Math.random() * 100);
        this.strictness = Math.ceil(Math.random() * 100);
    }

    get salary(): number {
        return this._salary;
    }

    set salary(value: number) {
        this._salary = value;
    }
}

export class Teacher extends Educator {}

export class Professor extends Educator implements IProfessor {
    doResearch(university: IUniversity, assistant: IStudent): void {
        assistant.learn();
        if (this.strictness + assistant.ability > 200) {
          console.log(`${this.name} has achieved significant success in research and joined a secret research institute!`);
          university.removeItem(this, ItemCategory.Teachers);
        }
    }
}
