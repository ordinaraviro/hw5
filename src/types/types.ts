import { IProfessor, IEducator } from "../interfaces/interfaces";

export type Lecturer = IEducator & IProfessor;
